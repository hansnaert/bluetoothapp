package hansnaert.vives.be.bluetoothapplication;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Set;


//activity to connect to
//1: open bluetooth adapter
//2: show paired devices or discover devices in a list
//3: when a device out of the list is selected, start second activity and pass the device as parameter


public class MainActivity extends AppCompatActivity {

    int REQUEST_ENABLE_BT = 1;

    boolean bluetoothEnabled = false;
    BluetoothAdapter mBluetoothAdapter;

    //objects of this class are shown in the listview => toString method is called to get the string to be placed in the list
    //class contains the bluetooth device, so the setOnItemClickListener has access to the bluetooth device
    class BluetoothDeviceWithNameToString
    {
        private BluetoothDevice device;
        public BluetoothDeviceWithNameToString(BluetoothDevice device)
        {
            this.device=device;
        }

        public String toString()
        {
            return device.getName();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
    }

    public void onOpenClick(View v) {
        Toast.makeText(this, "open", Toast.LENGTH_LONG).show();

        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBluetoothAdapter == null) {
            // Device does not support Bluetooth
        } else {
            if (!mBluetoothAdapter.isEnabled()) {
                Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
            } else
                bluetoothEnabled = true;
        }
        ((Button) findViewById(R.id.showPairedButton)).setEnabled(bluetoothEnabled);
        ((Button) findViewById(R.id.discoverDeviceButton)).setEnabled(bluetoothEnabled);
        ((Button) findViewById(R.id.openButton)).setEnabled(!bluetoothEnabled);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_ENABLE_BT) {
            if (resultCode == RESULT_OK)
                bluetoothEnabled = true;
            else
                bluetoothEnabled = false;
        }
        ((Button) findViewById(R.id.showPairedButton)).setEnabled(bluetoothEnabled);
        ((Button) findViewById(R.id.discoverDeviceButton)).setEnabled(bluetoothEnabled);
        ((Button) findViewById(R.id.openButton)).setEnabled(!bluetoothEnabled);    }

    ArrayList<BluetoothDeviceWithNameToString> list=new ArrayList<BluetoothDeviceWithNameToString>();
    ArrayAdapter<BluetoothDeviceWithNameToString> mArrayAdapter;

    @Override
    public void onStart()
    {
        super.onStart();
        mArrayAdapter=new ArrayAdapter<BluetoothDeviceWithNameToString>(this, android.R.layout.simple_list_item_1, list);
        ListView listView = (ListView) findViewById(R.id.listView);
        listView.setAdapter(mArrayAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long arg3) {
                view.setSelected(true);
                Toast.makeText(MainActivity.this,list.get(position).toString(),Toast.LENGTH_LONG).show();
                Intent i = new Intent(MainActivity.this, SecondActivity.class);
                i.putExtra("btdevice", list.get(position).device);
                startActivity(i);
            }
        });

        // Register the BroadcastReceiver
        IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
        registerReceiver(mReceiver, filter); // Don't forget to unregister during onDestroy

        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_COARSE_LOCATION)) {

                // Show an expanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                        1);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        }
    }

    public void onShowPairedClick(View v)
    {
        mArrayAdapter.clear();
        Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();
// If there are paired devices
        if (pairedDevices.size() > 0) {
            // Loop through paired devices
            for (BluetoothDevice device : pairedDevices) {
                // Add the name and address to an array adapter to show in a ListView
                mArrayAdapter.add(new BluetoothDeviceWithNameToString(device));
            }
        }
        else
            Toast.makeText(this,"no paired devices", Toast.LENGTH_LONG).show();
    }

    // Create a BroadcastReceiver for ACTION_FOUND
    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            // When discovery finds a device
            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                // Get the BluetoothDevice object from the Intent
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                // Add the name and address to an array adapter to show in a ListView
                mArrayAdapter.add(new BluetoothDeviceWithNameToString(device));
            }
        }
    };

    public void onDiscoverDevicesClick(View v)
    {
       /* if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {  // Only ask for these permissions on runtime when running Android 6.0 or higher
            int MY_PERMISSIONS_REQUEST_ACCESS_COARSE_LOCATION = 1;
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                    MY_PERMISSIONS_REQUEST_ACCESS_COARSE_LOCATION);
        }*/


        if(mBluetoothAdapter.isDiscovering())
            mBluetoothAdapter.cancelDiscovery();
        mArrayAdapter.clear();
        mBluetoothAdapter.startDiscovery();
    }

    @Override
    public void onDestroy()
    {
        if(mBluetoothAdapter.isDiscovering())
            mBluetoothAdapter.cancelDiscovery();
        unregisterReceiver(mReceiver);
        super.onDestroy();
    }
}
