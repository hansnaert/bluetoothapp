package hansnaert.vives.be.bluetoothapplication;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.UUID;

public class SecondActivity extends AppCompatActivity {

    private BluetoothDevice device;
    private BluetoothSocket socket;
    private InputStream inStream;
    private OutputStream outStream;

    //used to post a Runnable from a thread, to get executed in the UI thread
    //used for disabling and enabling the checkbox and buttons
    Handler handler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_child_toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        device=getIntent().getParcelableExtra("btdevice");
        ((TextView) findViewById(R.id.macTextView)).setText(device.getName());
    }

    public void onConnectClick(View v)
    {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    //SPP UUID for Bluetooth serial port: see http://developer.android.com/reference/android/bluetooth/BluetoothDevice.html#createRfcommSocketToServiceRecord(java.util.UUID)
                    socket=device.createRfcommSocketToServiceRecord(UUID.fromString("00001101-0000-1000-8000-00805F9B34FB"));
                    socket.connect();
                    inStream=socket.getInputStream();
                    outStream=socket.getOutputStream();
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            ((Button) findViewById(R.id.connectButton)).setEnabled(false);
                            ((CheckBox) findViewById(R.id.connectedCheckBox)).setChecked(socket.isConnected());
                        }
                    });
                }
                catch (IOException e) { }
            }
        }).start();
    }

    public void onSendClick(View v)
    {
        ((Button)findViewById(R.id.sendButton)).setEnabled(false);
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    outStream.write(new byte[]{0x41,0x42});
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            ((Button)findViewById(R.id.sendButton)).setEnabled(true);
                        }
                    });
                }
                catch (IOException e) { }
            }
        }).start();

    }


}
