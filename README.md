# Bluetooth App #

App for demonstrating how to:

* enable bluetooth adapter
* show paired devices
* discover devices
* connect to a serial bluetooth device
* send "AB" to the device

### What is this repository for? ###

* How to build an Android app that communicates with a Bluetooth serial device
* Version v1.0

### How do I get set up? ###

* Download and extract the repository
* Open project in Android studio

### Who do I talk to? ###

* Hans Naert